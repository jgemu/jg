PREFIX ?= /usr/local
DATAROOTDIR ?= $(PREFIX)/share
DOCDIR ?= $(DATAROOTDIR)/doc/jg
INCLUDEDIR ?= $(PREFIX)/include

SOURCEDIR := $(abspath $(patsubst %/,%,$(dir $(abspath $(lastword \
	$(MAKEFILE_LIST))))))

PKGCONFINCDIR := $(shell $(SOURCEDIR)/pkgconf.sh "$(PREFIX)" "$(INCLUDEDIR)")

.PHONY: all install uninstall

all:
	@echo "Please use the install or uninstall target"

install:
	@mkdir -p $(DESTDIR)$(INCLUDEDIR)/jg
	@mkdir -p $(DESTDIR)$(DATAROOTDIR)/pkgconfig
	@mkdir -p $(DESTDIR)$(DOCDIR)
	cp $(SOURCEDIR)/jg.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_2600.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_5200.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_7800.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_coleco.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_gb.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_gba.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_gg.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_intv.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_jag.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_lynx.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_md.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_n64.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_neogeo.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_nds.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_nes.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_ngp.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_pce.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_pcfx.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_psx.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_sms.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_snes.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_ss.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_vb.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_vectrex.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/jg_wswan.h $(DESTDIR)$(INCLUDEDIR)/jg/
	cp $(SOURCEDIR)/ChangeLog $(DESTDIR)$(DOCDIR)
	cp $(SOURCEDIR)/LICENSE $(DESTDIR)$(DOCDIR)
	cp $(SOURCEDIR)/README $(DESTDIR)$(DOCDIR)
	sed -e 's|@PREFIX@|$(PREFIX)|' -e 's|@INCLUDEDIR@|$(PKGCONFINCDIR)|' \
		$(SOURCEDIR)/jg.pc.in > $(DESTDIR)$(DATAROOTDIR)/pkgconfig/jg.pc

uninstall:
	rm -rf $(DESTDIR)$(INCLUDEDIR)/jg/
	rm -rf $(DESTDIR)$(DOCDIR)
	rm -f $(DESTDIR)$(DATAROOTDIR)/pkgconfig/jg.pc
