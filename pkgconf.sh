#!/bin/sh

set -eu

prefix="$1"
includedir="$2"

case "$includedir" in
  "$prefix"* )
    include_path="\${prefix}${includedir#$prefix}"
  ;;
  * )
    include_path="$includedir"
  ;;
esac

printf %s\\n "$include_path"
