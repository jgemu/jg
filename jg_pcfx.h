/*
zlib License

Copyright (c) 2020-2025 Rupert Carmichael

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#ifndef JG_PCFX_H
#define JG_PCFX_H

enum jg_pcfx_input_type {
    JG_PCFX_UNCONNECTED,
    JG_PCFX_PAD,
    JG_PCFX_MOUSE
};

static const char *jg_pcfx_input_name[] = {
    "Unconnected",
    "PC-FX Control Pad",
    "PC-FX Mouse"
};

// PC-FX Control Pad
#define NDEFS_PCFXPAD 14
static const char *defs_pcfxpad[NDEFS_PCFXPAD] = {
    "Up", "Down", "Left", "Right", "Select", "Run",
    "I", "II", "III", "IV", "V", "VI", "Mode1", "Mode2"
};

// PC-FX Mouse
#define NDEFS_PCFXMOUSE 2
static const char *defs_pcfxmouse[NDEFS_PCFXMOUSE] = {
    "Left", "Right"
};

static jg_inputinfo_t jg_pcfx_inputinfo(int index, int type) {
    jg_inputinfo_t ret;
    switch (type) {
        case JG_PCFX_PAD: {
            ret.type = JG_INPUT_CONTROLLER;
            ret.index = index;
            ret.name = index == 0 ? "pcfxpad1" : "pcfxpad2";
            ret.fname = jg_pcfx_input_name[type];
            ret.defs = defs_pcfxpad;
            ret.numaxes = 0;
            ret.numbuttons = NDEFS_PCFXPAD;
            return ret;
        }
        case JG_PCFX_MOUSE: {
            ret.type = JG_INPUT_POINTER;
            ret.index = index;
            ret.name = "pcfxmouse";
            ret.fname = jg_pcfx_input_name[type];
            ret.defs = defs_pcfxmouse;
            ret.numaxes = 0;
            ret.numbuttons = NDEFS_PCFXMOUSE;
            return ret;
        }
        default: {
            ret.type = JG_INPUT_EXTERNAL;
            ret.index = index;
            ret.name = "unconnected";
            ret.fname = jg_pcfx_input_name[0];
            ret.defs = NULL;
            ret.numaxes = ret.numbuttons = 0;
            return ret;
        }
    }
}

#endif
